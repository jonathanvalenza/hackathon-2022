import { Product } from "../entities-interfaces/Product";

export interface ProductRepositoryInterface {
    getProducts: () => Promise<Product[]>;
  }