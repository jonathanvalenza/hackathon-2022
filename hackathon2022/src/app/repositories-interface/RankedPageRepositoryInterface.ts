import { RankedPage } from "../entities-interfaces/RankedPage";

export interface RankedPageRepositoryInterface {
   findRankedPagesForProduct: (name: String) => Promise<RankedPage[]>;
}