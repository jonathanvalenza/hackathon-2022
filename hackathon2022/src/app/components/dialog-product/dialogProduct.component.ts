import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: "dialogProduct",
  templateUrl: "./dialogProduct.component.html",
  styleUrls: ["./dialogProduct.component.css"]
})
export class DialogProductComponent implements OnInit {

  public visiScore: number = 0;
  public pages: number = 0;
  public name: string = "[name]";
  public photoUrl: string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { 
      product: any},
    public dialogRef: MatDialogRef<DialogProductComponent>
    ) { }

  ngOnInit(): void {
    console.log("product dans le dial final : ", this.data);
    this.generateName();
    this.generateTotalPages();
    this.generateTotalVisiscore();
    this.generatePhoto();
  }

  generateTotalVisiscore() {
    if (this.name != "nom manquant" && this.data.product.rankedPages != undefined){
      this.visiScore = this.data.product.rankedPages.reduce((item: any) => {
        return item.accesses.visibilityScore;
      });
    }
  }

  generateTotalPages() {
    if(this.data.product.rankedPages !== undefined) {
      this.pages = this.data.product.rankedPages.length;
    }
  }

  generateName() {
    this.name = this.data.product.name != undefined ? this.data.product.name : "nom manquant";
  }

  generatePhoto() {
    if(this.data.product.imageUrl !== undefined) {
      this.photoUrl =  this.data.product.imageUrl;
    }
  }

}


