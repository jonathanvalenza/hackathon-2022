import { Component, OnInit, Inject } from "@angular/core";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: "dialogConfirm",
  templateUrl: "./dialogConfirm.component.html",
  styleUrls: ["./dialogConfirm.component.css"]
})
export class DialogConfirmComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: {
      title: any; 
      content: any},
    public dialogRef: MatDialogRef<DialogConfirmComponent>
    ) { }

  ngOnInit(): void {
    console.log("data dans le confirm", this.data);

  }

  validate(){
    this.dialogRef.close(true);
  }

}
