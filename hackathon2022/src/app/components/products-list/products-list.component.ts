import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { GetProductsService } from '../../services/get-products.service';
import { Product } from '../../entities-interfaces/Product';
import { MatDialog } from "@angular/material/dialog";
import { DialogConfirmComponent } from '../dialog-confirm/dialogConfirm.component';
import { DialogProductComponent } from '../dialog-product/dialogProduct.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  public displayedColumns: string[] = ['imageUrl', 'name', 'afficher'];
  // public displayedColumnsNames: string[] = ['imageUrl', 'imageUrl', 'afficher'];
  public products: any[] | null = null;
  public dataSource: MatTableDataSource<any[]>;

  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private getProductsService: GetProductsService,
    private matDialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.getProductsService.handle().then((products: Product[]) => {
      console.log("Products : ", products);
      this.products = products
      this.dataSource = new MatTableDataSource(this.products);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    })

    // this.dataSource.paginator = this.paginator;
    
    
  }
  
  ngAfterViewInit (){
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDial(product: any) {

    this.matDialog.open(DialogConfirmComponent, {
			data: { title: "Voulez-vous afficher le produit ?" }
        }).afterClosed().subscribe((confirm) => {
            if(confirm) {
              this.question2(product)
            }
    });   
  }

  question1(product: any){
    this.matDialog.open(DialogConfirmComponent, {
			data: { title: "Êtes-vous développeur ?" }
        }).afterClosed().subscribe((confirm) => {
            if(confirm) {
              // question 1
              console.log("question 2 : ", product);
              this.question2(product)
            }
    });
  }

  question2(product: any){
    this.matDialog.open(DialogConfirmComponent, {
			data: { title: "Êtes-vous un bon développeur ?" }
        }).afterClosed().subscribe((confirm) => {
            if(confirm) {
              // question 1
              console.log("question 3 : ", product);
              this.question3(product)
            }
    });
  }

  question3(product: any){
    this.matDialog.open(DialogConfirmComponent, {
			data: { title: "Êtes-vous sur de la réponse précédente ?" }
        }).afterClosed().subscribe((confirm) => {
            if(confirm) {
              this.matDialog.open(DialogProductComponent, {
                data: { product }
              });
            }
    });
  }

}


