import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { VaMatTableComponent } from './components/va-mat-table/va-mat-table.component';
//Material
import { AngularMaterialModule } from './modules/material.module';
import { ProductsListComponent } from './components/products-list/products-list.component';
import { ColumnSorterComponent } from './components/va-mat-table/actions/column-sorter/column-sorter.component';
import { DialogConfirmComponent } from './components/dialog-confirm/dialogConfirm.component';
import { DialogProductComponent } from './components/dialog-product/dialogProduct.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    VaMatTableComponent,
    ColumnSorterComponent,
    DialogConfirmComponent,
    DialogProductComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
