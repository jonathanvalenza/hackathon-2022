import { Component } from '@angular/core';
import { Product } from './entities-interfaces/Product';
import { GetProductsService } from './services/get-products.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hackathon2022';

  constructor(private getProductsService: GetProductsService) {
    
  }

  ngOnInit(): void {
  }


}
