export interface RankedPage {
    url: string;
    shopName: string;
    accesses: Access[];
}

export interface Access {
    keyword: string;
    monthlySearchCount: number;
    rank: number;
    visibilityScore: number;
}