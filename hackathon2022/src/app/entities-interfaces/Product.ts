import { RankedPage } from "./RankedPage";

export interface Product {
    id: number;
    name: string;
    keywords?: string[];
    imageUrl?: string;
    rankedPages?: RankedPage[];

    brandOwner?: string;
    creator?: string;
}