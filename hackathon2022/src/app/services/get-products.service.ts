import { Injectable } from '@angular/core';
import { Product } from '../entities-interfaces/Product';
import { ProductRepositoryInterface } from '../repositories-interface/ProductRepositoryInterface';
import { RankedPageRepositoryInterface } from '../repositories-interface/RankedPageRepositoryInterface';
import { ProductRepository } from '../repositories/ProductRepository';
import { RankedPageRepository } from '../repositories/RankedPageRepository';


@Injectable({
  providedIn: 'root'
})
export class GetProductsService {

  private productRepository: ProductRepositoryInterface;
  private rankedPageRepository: RankedPageRepositoryInterface;

  constructor() { 
    this.productRepository = new ProductRepository();
    this.rankedPageRepository = new RankedPageRepository();
  }

  handle(): Promise<Product[]> {
    
    return this.productRepository.getProducts().then(products => {
        products?.map(async product => {
          product.rankedPages = await this.rankedPageRepository.findRankedPagesForProduct(product.name);
        });
        return products;
      }
    );
  }

  // FOR TESTS ONLY
  setProductRepository(productRepository: ProductRepositoryInterface) {
    this.productRepository = productRepository;
  }
  
  setRankedPageRepository(rankedPageRepository: RankedPageRepository) {
    this.rankedPageRepository = rankedPageRepository;
  }
}
