import { Injectable } from '@angular/core';
declare var require: any;

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  styleTag: any;
  theme: string | null = 'blue';
  isDark: string | null = 'false';
  themeData: any;

  constructor() {
      this.createStyle();
      this.setTheme();
  }

  private createStyle() {
      const head = document.head || document.getElementsByTagName('head')[0];
      this.styleTag = document.createElement('style');
      this.styleTag.type = 'text/css';
      this.styleTag.id = 'appthemes';
      head.appendChild(this.styleTag);
  }

  public setTheme() {

    if (!localStorage.getItem('theme')) {
      localStorage.setItem('theme', 'blue');
    } else {
      this.theme = localStorage.getItem('theme')
    }

    if (!localStorage.getItem('dark')) {
      localStorage.setItem('dark', 'false');
    } else {
      this.isDark = localStorage.getItem('dark');
    }

    if (this.isDark == 'false') {
      this.themeData = require('../themes/'+this.theme+'-theme.scss');
      document.getElementsByTagName('body')[0].className = this.theme+'-theme';
    } else {
      this.themeData = require('../themes/'+this.theme+'-dark-theme.scss');
      document.getElementsByTagName('body')[0].className = this.theme+'-dark-theme';
    }

    this.injectStylesheet(this.themeData);
    
  }

  injectStylesheet(css: any) {
      this.styleTag.innerHTML = css.default;
  }
}


