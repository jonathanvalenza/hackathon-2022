import { TestBed } from '@angular/core/testing';
import { RankedPage } from '../entities-interfaces/RankedPage';
import { ProductRepository } from '../repositories/ProductRepository';
import { RankedPageRepository } from '../repositories/RankedPageRepository';

import { GetProductsService } from './get-products.service';

describe('GetProductsService', () => {
  let service: GetProductsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetProductsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getProductsShouldReturnProductsWhenAvailable', async () => {
    const product1 = {id: 1, name: "Nutella", rankedPages: []};
    const product2 = {id: 2, name: "Lait", rankedPages: []};

    const inMemoryProductQueryImpl = new ProductRepository();
    inMemoryProductQueryImpl.setProducts([product1, product2]);

    service = new GetProductsService();
    service.setProductRepository(inMemoryProductQueryImpl);

    const listSize = await service.handle();

    expect(listSize.length).toEqual(2);
  });

  it('getProductsShouldReturnAProductWithAListOfShops', async () => {
    const product1 = {id: 1, name: "nutella", rankedPages: []};
    const product2 = {id: 2, name: "Lait", rankedPages: []};

    const rankedPage1 = {
      url: "page_url_for_nutella",
      shopName: "auchan",
      accesses: [{
        keyword: "keyword1",
        monthlySearchCount: 10,
        rank: 1,
        visibilityScore: 10,
      }]
    } as RankedPage;

    const rankedPage2 = {
      url: "page_url_for_nutella",
      shopName: "carrefour",
      accesses: [{
        keyword: "keyword1",
        monthlySearchCount: 10,
        rank: 1,
        visibilityScore: 10,
      }]
    } as RankedPage;

    service = new GetProductsService();
    
    const productRepository = new ProductRepository();
    productRepository.setProducts([product1, product2]);

    const rankedPageRepository = new RankedPageRepository();
    rankedPageRepository.setRankedPages([rankedPage1, rankedPage2]);
    rankedPageRepository.findRankedPagesForProduct("Nutella");

    service = new GetProductsService()
    service.setProductRepository(productRepository);
    service.setRankedPageRepository(rankedPageRepository);

    const listSize = await service.handle();
    expect(listSize.length).toEqual(2);
    
    const list = await service.handle();
    expect(list[0]).toEqual({id: 1, name: "nutella", rankedPages: [
      {
        url: "page_url_for_nutella",
        shopName: "auchan",
        accesses: [{
          keyword: "keyword1",
          monthlySearchCount: 10,
          rank: 1,
          visibilityScore: 10,
        }]
      },
      {
        url: "page_url_for_nutella",
        shopName: "carrefour",
        accesses: [{
          keyword: "keyword1",
          monthlySearchCount: 10,
          rank: 1,
          visibilityScore: 10,
        }]
      }
      
    ]});
  });

});


