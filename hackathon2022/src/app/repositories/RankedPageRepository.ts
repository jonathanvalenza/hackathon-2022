import { AxiosError, AxiosResponse } from "axios";
import { Access, RankedPage } from "../entities-interfaces/RankedPage";
import { RankedPageRepositoryInterface } from "../repositories-interface/RankedPageRepositoryInterface";
import { axios } from "./AxiosHttpClient";

export interface AccessData {
    keyword: string;
    monthlySearchCount: number;
    rank: number;
    visibilityScore: number;
}

export interface RankedPageData {
    url: string;
    domainName: string;
    access: AccessData[]
}

export class RankedPageRepository implements RankedPageRepositoryInterface {

    private urlAPI = "https://localhost:7012/RankedPage";



    public async findRankedPagesForProduct(name: String): Promise<RankedPage[]> {

      if (name == undefined) {
        return [];
      }

      var tmp = this.urlAPI;
      name!.split(" ").map((item, index) => {
        if (index < 2)
        {
          tmp += ((index == 0 ? "?" : "&") + "MultipleNames=" + item)
        }
      });

      console.log(tmp);
      var res = await axios.get(tmp).then((response: AxiosResponse) => {
        const formatedRankedPages: RankedPage[] = [];
        console.log("axios responce", response)
              response.data.rankedPages == undefined ? [] : response.data.rankedPages.map((item: RankedPageData) => {
                      const rankedPage = {
                          url: item.url,
                          shopName: item.domainName,
                          accesses: this.formatAccessDataList(item.access),
                      } as RankedPage;
                      formatedRankedPages.push(rankedPage);
                  });
              console.log("formatedRankedPages" + formatedRankedPages)
              return formatedRankedPages;
          }).catch((error: AxiosError) => {
              console.log("RankedPageRepository error" + error)
          });

          return Promise.resolve(res);
    };

    private formatAccessDataList(accessDataList: AccessData[]): Access[] {
        return accessDataList.map((accessData: AccessData) => this.formatAccessData(accessData));
    }

    private formatAccessData(accessData: AccessData): Access {
        return {
            keyword: accessData.keyword,
            monthlySearchCount: accessData.monthlySearchCount,
            rank: accessData.rank,
            visibilityScore: accessData.visibilityScore,
        } as Access;

    }

    // FOR TESTS ONLY
    private rankedpages: RankedPage[] = [];

    setRankedPages(rankedPages: RankedPage[]) {
        this.rankedpages = rankedPages;

    }

}
