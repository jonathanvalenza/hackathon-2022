import { AxiosError, AxiosResponse } from "axios";
import { Product } from "../entities-interfaces/Product";
import { ProductRepositoryInterface } from "../repositories-interface/ProductRepositoryInterface";
import { axios } from "./AxiosHttpClient";

interface ProductData {
    _id: string;
    _keywords: string[];
    abbreviated_product_name_fr: string;
    brand_owner: string;
    creator: string;
    image_url: string;
}

// API: https://world.openfoodfacts.org/api/v2/search

export class ProductRepository implements ProductRepositoryInterface {

    private urlApi = "https://world.openfoodfacts.org/api/v2/search";
    // https://fr.openfoodfacts.org/categorie/eaux.json
    // private urlApi = "https://fr.openfoodfacts.org/categorie/biscuit.json";

    
    public getProducts(): Promise<Product[]> {
        
        return axios.get(this.urlApi)
            .then((response: AxiosResponse) => {
                
                const formatedProducts: Product[] = [];
                
                response.data.products
                    .map((item: ProductData) => {
                        
                        const product = {
                            id: parseInt(item._id),
                            keywords: item._keywords,
                            name: item.abbreviated_product_name_fr,
                            brandOwner: item.brand_owner,
                            creator: item.creator,
                            imageUrl: item.image_url,
                        } as Product;
                        formatedProducts.push(product);
                    });
                    return formatedProducts;
            })
            .catch((error: AxiosError) => {
                console.log("ProductRepository " + error.message);
            });
    }


    // FOR TESTS ONLY
    private products: Product[] = [];

    setProducts(products: Product[]) {
        this.products = products;
    }
    
}