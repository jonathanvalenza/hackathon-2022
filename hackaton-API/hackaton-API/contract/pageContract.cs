﻿using System;
using Newtonsoft.Json;

// using 

namespace hackaton_API.contract
{
    public class pageContract
    {
        [JsonProperty("domainName")]
        public string? DomainName { get; set; }
        [JsonProperty("url")]
        public string? Url { get; set; }
        [JsonProperty("access")]
        public List<key>? Access { get; set; }
    }

    public class key
    {
        [JsonProperty("rank")]
        public int Rank { get; set; }
        [JsonProperty("visibilityScore")]
        public int Visibility_score { get; set; }
        [JsonProperty("keyword")]
        public string? Keyword { get; set; }
        [JsonProperty("monthlySearchCount")]
        public int MonthlySearchCount { get; set; }
    }

    public class rawData
    {
        public string? DomainName { get; set; }
        public string? Url { get; set; }
        public int Rank { get; set; }
        public int Visibility_score { get; set; }
        public string? Keyword { get; set; }
        public int MonthlySearchCount { get; set; }

        public bool Equals(rawData other)
        {
            if (Object.ReferenceEquals(other, null)) return false;
            if (Object.ReferenceEquals(this, other)) return true;

            return Url != null ? Url.Equals(other.Url) : false ;
        }

        public override int GetHashCode()
        {
            return Url == null ? 0 : Url.GetHashCode();
        }
    }
}

