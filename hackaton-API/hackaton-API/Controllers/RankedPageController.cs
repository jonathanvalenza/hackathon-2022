﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using hackaton_API.classes;
using hackaton_API.contract;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace hackaton_API.Controllers
{
    [Route("[controller]")]
    public class RankedPageController : Controller
    {
        private DatabaseManager DBManager;
        private IEnumerable<IDDO> CurrentDommains;


        public RankedPageController()
        {
            DBManager = new DatabaseManager();
            CurrentDommains = getDomain();
        }

        private class IDDO
        {
            public int ID { get; set; }
            public string Domain { get; set; }
        }

        private int FindEnd(string input)
        {
            if (input.EndsWith(".fr")) return input.Length - 3 - 12;
            if (input.EndsWith(".com")) return input.Length - 4 - 12;
            else return input.Length - 12;
        }

        private string findDomain(int input)
        {
            return CurrentDommains.Where((s) => s.ID == input).FirstOrDefault().Domain;
        }

        private IEnumerable<IDDO> getDomain()
        {
            var Website = DBManager.getConnection();

            var Query  = "select * from website";

            var ret = new List<IDDO>();
            using (Website)
            {
                if (Website.State == ConnectionState.Closed)
                {
                    Website.Open();
                }
                using (MySqlCommand cmd = new MySqlCommand(Query, Website))
                {
                    var Data = cmd.ExecuteReader();
                    while (Data.Read())
                    {
                        IDDO tmp = new IDDO()
                        {
                            ID = (int)Data.GetValue(0),
                            Domain = Data.GetValue(1).ToString().Substring(12, FindEnd(Data.GetValue(1).ToString()))
                        };

                        ret.Add(tmp);
                    }
                    Data.Close();
                }
            }

            return ret; 
        }


        private string MakeQuery(List<string> MultipleNames)
        {
            var Query = "select * from ranked_page as RP left join keyword as K on RP.keyword_id = K.id";
            if (MultipleNames.Count() > 0)
            {
                Query += " Where ";
            }
            MultipleNames.ForEach((s) =>
            {
                Query += "url LIKE '%" + s + "%' " + "OR ";
            });
            if (MultipleNames.Count() > 0)
            {
                Query = Query.Substring(0, Query.Length - 4) + ";";
            }
            return Query;
        }

        [EnableCors("Origins")]
        [HttpGet(Name = "rankedpage")]
        public List<pageContract> Get(IEnumerable<string> MultipleNames)
        {
            var ret = new List<pageContract>();
            if (MultipleNames.Count() == 0)
            {
                return ret;
            }
            if (String.IsNullOrEmpty(MultipleNames.ToList()[0]))
            {
                return ret;
            }
            var RankedPage = DBManager.getConnection();

            //TODO faire un filtre avec AND AND AND ..... dans une fonction a part
            var Query = MakeQuery(MultipleNames.ToList());

            // recupertaion
            var unformatedData = new List<rawData>();
            using (RankedPage)
            {
                if (RankedPage.State == ConnectionState.Closed)
                {
                    RankedPage.Open();
                }
                using (MySqlCommand cmd = new MySqlCommand(Query, RankedPage))
                {
                    var Data = cmd.ExecuteReader();
                    while (Data.Read())
                    {   
                        rawData currentData = new rawData()
                        {
                            DomainName = findDomain(Data.GetInt32(4)),
                            Url = Data.GetString(1),
                            Rank = Data.GetInt32(2),
                            Visibility_score = Data.GetInt32(3),
                            Keyword = Data.GetString(7),
                            MonthlySearchCount = Data.GetInt32(8),
                        };

                        unformatedData.Add(currentData);
                    }
                    Data.Close();
                }
            }

            // formation
            var DunformatedData = unformatedData.GroupBy(x => x.Url).Select(g => g.First()).ToList();

            DunformatedData.ForEach((data) => {
                var current = new pageContract();
                current.DomainName = data.DomainName;
                current.Url = data.Url;
                current.Access = new List<key>();
                ret.Add(current);
            });

            ret.ForEach((current) => {
                unformatedData.ForEach((data) =>
                {
                    if (data.Url == current.Url)
                    {
                        if (current.Access == null) { current.Access = new List<key>(); }
                        current.Access.Add(new key()
                        {
                            Rank = data.Rank,
                            Visibility_score = data.Visibility_score,
                            Keyword = data.Keyword,
                            MonthlySearchCount = data.MonthlySearchCount,
                        });
                    }
                });
            });
            return ret;
        }
    }
}

