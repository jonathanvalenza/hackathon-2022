﻿using Microsoft.AspNetCore.Mvc;

using hackaton_API.classes;
namespace hackaton_API.Controllers;

[ApiController]
[Route("[controller]")]
public class HelloWorldController : ControllerBase
{
    public HelloWorldController(){}

    /// <summary>
    /// Hello world test
    /// </summary>
    /// <returns></returns>
    [HttpGet(Name = "HelloWorld")]
    public string Get()
    {
        //var test = DBManager.GetReader("select * from website");
        return "hello World";
    }
}

