﻿using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace hackaton_API.classes
{
	public class DatabaseManager
	{
		public string m_strMySQLConnectionString = "Server='212.47.236.153';Port=9622; Database=market_ranking; User Id=random; Password=G!EPpFf8SX;";

		public DatabaseManager()
		{

		}

        public MySqlConnection getConnection()
        {
            return new MySqlConnection(m_strMySQLConnectionString);
        }

        public MySqlDataReader GetReader(string Query)
        {
            string strData = "";
            MySqlConnection ret = getConnection();

            try
            {
                if (string.IsNullOrEmpty(Query) == true)
                    return null;

                using (ret)
                {
                    if (ret.State == ConnectionState.Closed)
                    {
                        ret.Open();
                    }
                    using (MySqlCommand cmd = new MySqlCommand(Query, ret))
                    {
                        return cmd.ExecuteReader();
                    }
                    ret.Close();
                }
            }
            catch
            {
                if (ret.State != ConnectionState.Closed)
                {
                    ret.Close();
                }
                return null;
            }
            finally
            {

            }
        }
    }
}

